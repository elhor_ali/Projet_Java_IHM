package constructions;

public class Porte {
	/* une porte relie deux pieces entre elles */
	/*********** attributs **************/
	private int id;
	private Salle salle1;
	private Salle salle2;
	private String emplacementPorteDansSalle1;
	private String emplacementPorteDansSalle2;
	private boolean estVerrouillee; /* indique si la porte est verrouillée ou non */
	/*********** constructeur **************/
	public Porte(int id, Salle salle1, String emplacementPorteDansSalle1, Salle salle2,String emplacementPorteDansSalle2) {
		super();
		this.id = id;
		this.salle1 = salle1;
		this.salle2 = salle2;
		this.emplacementPorteDansSalle1 = emplacementPorteDansSalle1;
		this.emplacementPorteDansSalle2 = emplacementPorteDansSalle2;

	}
	/*********** geters **************/
	public int getId() {
		return id;
	}

	public String getEmplacementPorteDansSalle1() {
		return emplacementPorteDansSalle1;
	}

	public void setEmplacementPorteDansSalle1(String emplacementPorteDansSalle1) {
		this.emplacementPorteDansSalle1 = emplacementPorteDansSalle1;
	}

	public String getEmplacementPorteDansSalle2() {
		return emplacementPorteDansSalle2;
	}
	
	public Salle getSalle1() {
		return salle1;
	}

	public Salle getSalle2() {
		return salle2;
	}

	/*********** seters **************/
	

	public void setEmplacementPorteDansSalle2(String emplacementPorteDansSalle2) {
		this.emplacementPorteDansSalle2 = emplacementPorteDansSalle2;
	}



	
}
