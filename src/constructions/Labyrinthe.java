package constructions;

import java.util.ArrayList;
import java.util.Arrays;

import protagonistes.Guerrier;
import protagonistes.Monstre;

public class Labyrinthe {
	/*********** attributs **************/
	private Guerrier guerrier;

	private Salle[] salles = new Salle[11]; /* la liste des pieces */

	private Porte[] portes = new Porte[10]; /* la liste des portes */

	private int emplacementGuerrier; /* le numéro de salle ou se trouve le guerrier */
	
	/*********** constructeur **************/
	public Labyrinthe(Guerrier guerrier) {
		super();

		/* Creation des salles */

		for (int i = 0; i < 10; i++) {
			if (i == 0) {
				this.salles[i] = new Salle(i, null);
			} else {
				this.salles[i] = new Salle(i, new Monstre());
			}

		}

		/* Creation des portes */

		this.portes[0] = new Porte(0, salles[0], "nord", salles[1], "sud");
		this.portes[1] = new Porte(1, salles[0], "est", salles[2], "ouest");
		this.portes[2] = new Porte(2, salles[2], "est", salles[3], "ouest");
		this.portes[3] = new Porte(3, salles[3], "nord", salles[4], "sud");
		this.portes[4] = new Porte(4, salles[4], "ouest", salles[5], "est");
		this.portes[5] = new Porte(5, salles[4], "nord", salles[6], "sud");
		this.portes[6] = new Porte(6, salles[4], "est", salles[7], "ouest");
		this.portes[7] = new Porte(7, salles[7], "est", salles[8], "ouest");
		this.portes[8] = new Porte(8, salles[8], "sud", salles[9], "nord");
		this.portes[9] = new Porte(9, salles[9], "sud", salles[10], "nord");

		/* creation du guerrier */

		this.guerrier = guerrier;

		this.emplacementGuerrier = 0;
	}

	/************* gets **************/

	public Guerrier getGuerrier() {
		return guerrier;
	}

	/*recuperer une salle*/
	public Salle getSalles(int i) {
		return salles[i];
	}

	public void setSalles(Salle[] salles) {
		this.salles = salles;
	}

	public Porte getPortes(int i) {
		return portes[i];
	}

	public void setPortes(Porte[] portes) {
		this.portes = portes;
	}

	public int getEmplacementGuerrier() {
		return emplacementGuerrier;
	}



	/*************** sets *************/
	public void setEmplacementGuerrier(int emplacementGuerrier) {
		this.emplacementGuerrier = emplacementGuerrier;
	}

	public void setGuerrier(Guerrier guerrier) {
		this.guerrier = guerrier;
		this.emplacementGuerrier = 0;
	}

}
