package vue;

import control.*;
import protagonistes.*;
/*cette classe represente le boundary de la creation dun guerrier*/

public class CreerGuerrierBoundary {
	
	private CreerCombattantController controleurC;

	public CreerGuerrierBoundary(CreerCombattantController controleurC) {
		super();
		this.controleurC = controleurC;
		
	}

	public String creerGuerrier(String nom) {
		
		/* demander le nom du guerrier */
	
		TypeCombattant typeCombattant = TypeCombattant.GUERRIER;

	
		controleurC.creerCombattant(typeCombattant, nom);

		return "bienvenue " + nom;
	}

}
