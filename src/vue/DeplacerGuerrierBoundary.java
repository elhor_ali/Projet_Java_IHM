package vue;

import control.CombattreController;
import control.DeplacerGuerrierController;

/*Cette classe permet � l'utilisateurde se deplacer ou de rester dans une salle
 * 
 * Si le guerrier decide de rester dans la salle donc on continue le combat
 * */


public class DeplacerGuerrierBoundary {
	private DeplacerGuerrierController controleur;
	private CombattreController controlleurCombatt;

	public DeplacerGuerrierBoundary(DeplacerGuerrierController controleur, CombattreController controlleurCombatt) {
		super();
		this.controleur = controleur;
		this.controlleurCombatt = controlleurCombatt;

	}

	public String getNomGuerrier() {
		return this.controleur.getLabyrinthe().getGuerrier().getNomGuerrier();
	}
	
	public String infosGuerrier() {
		String txt;
		/*recuperer le nom du guerrier*/
		
			String nomGuerrier = getNomGuerrier();
			
			/*recuperer l'emplacement du guerrier dans la labyrinthe*/
			int numeroSalleCourante = this.controleur.getLabyrinthe().getEmplacementGuerrier();
			txt="Le guerrier " + nomGuerrier + " est dans la salle " + numeroSalleCourante + " \n";
		
		
		
			
			return txt;
		
	}
	
	
	public String donnerAction() {
		
		return "\n Veuillez saisir votre prochain Deplacement : ";
		
	}
	
	public String ExecEtAfficheAction(int dirrection) {
		String nomGuerrier = this.controleur.getLabyrinthe().getGuerrier().getNomGuerrier();
		String dir=controleur.deplacerGuerrier(dirrection);
		if (dir != null) {

			return "Le guerrier " + nomGuerrier + " se deplace vers le " + dir + "\n";
//				/*D�s la rentr�e � une salle on commence le combat (s'il existe un monstre ) */
			
	//
		} else {
			return "Le guerrier " + nomGuerrier + " ne trouve pas de porte pour se deplacer" + "\n";
		}
		
		
	}
	


}
