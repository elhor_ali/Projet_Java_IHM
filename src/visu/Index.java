package visu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import control.CombattreController;
import control.CreationLabyrintheController;
import control.CreerCombattantController;
import control.DeplacerGuerrierController;
import vue.CreerGuerrierBoundary;
import vue.DeplacerGuerrierBoundary;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;
import java.awt.Font;

public class Index extends JFrame {

	private JPanel contentPane;
	private JTextField nom;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Index frame = new Index();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Index() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		nom = new JTextField();
		nom.setBounds(165, 117, 123, 20);
		contentPane.add(nom);
		nom.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Veuillez saisir un nom pour votre guerrier");
		lblNewLabel_1.setFont(new Font("Verdana", Font.BOLD, 15));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(43, 39, 351, 42);
		contentPane.add(lblNewLabel_1);
		
		JButton valider = new JButton("valider");
		valider.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
				Debut debut = new Debut(nom.getText());
				debut.setVisible(true);
			}
		});
		valider.setBounds(165, 166, 123, 23);
		contentPane.add(valider);
		
		JButton reprendre = new JButton("Reprendre");
		reprendre.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Debut debut = new Debut("@_RePrEnDre_@");
				debut.setVisible(true);
			}
		});
		reprendre.setBounds(165, 204, 123, 23);
		contentPane.add(reprendre);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Index.class.getResource("/images/Capture.PNG")));
		lblNewLabel.setBounds(5, 5, 424, 245);
		contentPane.add(lblNewLabel);
	}
}
