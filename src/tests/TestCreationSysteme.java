package tests;



import control.*;
import vue.*;


public class TestCreationSysteme {
	public static void main(String[] args) {
       
		CreationLabyrintheController controleurL = new CreationLabyrintheController();
		CreerCombattantController controleurC = new CreerCombattantController(controleurL);
		CreerGuerrierBoundary boundaryC = new CreerGuerrierBoundary(controleurC);
		System.out.println(boundaryC.creerGuerrier("ali"));
		// controleurC.afficher();
		CombattreController controlleurCombatt = new CombattreController(controleurL.getLabyrinthe());
		DeplacerGuerrierController controleurD = new DeplacerGuerrierController(controleurL.getLabyrinthe());
		DeplacerGuerrierBoundary boundaryD = new DeplacerGuerrierBoundary(controleurD, controlleurCombatt);

	//	boundaryD.deplacerGuerrier();

	}
}
