/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package protagonistes;

/**
 *
 * Classe mere de guerrier et monstre
 * 
 */
abstract public class Combattant {

	protected int pointsDeVieMax;
	protected int pointsDeVie;

	public Combattant(int pointsDeVie) {
		this.pointsDeVie = pointsDeVie;
		this.pointsDeVieMax = pointsDeVie;
	}

	public int getPointsDeVie() {
		return pointsDeVie;
	}

	public void setPointsDeVie(int pointsDeVie) 
	{
		this.pointsDeVie = pointsDeVie;
	}
	
    /*donner un coup � un autre combattant*/
	
	public void donnerCoup(Combattant combattant) {
		
		combattant.prendreCoup(1);
		
	}

	/*prendre un coup de la part d'un autre combatant*/
	
	public void prendreCoup(int puissanceCoup) {
		
		if (this.pointsDeVie - puissanceCoup <= 0) {
			this.pointsDeVie = 0;
			

		} else {
			this.pointsDeVie -= puissanceCoup;
			
		}

		
	}

	public int getPointsDeVieMax() {
		return pointsDeVieMax;
	}

	public void setPointsDeVieMax(int pointsDeVieMax) {
		this.pointsDeVieMax = pointsDeVieMax;
	}

}
