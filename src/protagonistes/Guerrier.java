package protagonistes;

import tresor.Arme;
import tresor.Armure;

public class Guerrier extends Combattant{
    private String nomGuerrier;
    private Arme arme;
    private Armure armure;
    private int or;
    
    
    public Guerrier(String nomGuerrier) {
        super(10);
        this.nomGuerrier = nomGuerrier;
        this.or=0;
        
        
    }
    
    public Guerrier(String nomGuerrier,int puissance,int or,Arme arme,Armure armure) {
        super(puissance);
        this.nomGuerrier = nomGuerrier;
        this.or=or;
        this.arme=arme;
        this.armure=armure;
        
    }
    
    public Arme getArme() {
		return arme;
	}

	public void setArme(Arme arme) {
		this.arme = arme;
	}

	public Armure getArmure() {
		return armure;
	}

	public void setArmure(Armure armure) {
		this.armure = armure;
	}

	public int getOr() {
		return or;
	}

	public void setOr(int or) {
		this.or = or;
	}

	

  

    public String getNomGuerrier() {
        return nomGuerrier;
    }
    
    
  
    
    @Override
    public void donnerCoup(Combattant monstre)
    {
    	int puissanceCoup = 1;
    	if (arme!=null) puissanceCoup+=arme.getValeur();
    	monstre.prendreCoup(puissanceCoup);
    	
    }
    
    
    @Override
    public void prendreCoup(int puissanceCoup)
    {
    	if (this.armure!=null) 
    	{
    		if (this.armure.getValeur()-puissanceCoup<=0) 
    		{
				puissanceCoup=puissanceCoup-this.armure.getValeur();
				this.armure.setValeur(0);
			}
    		else
    		{
    			this.armure.setValeur((this.armure.getValeur() - puissanceCoup));
    			puissanceCoup=0;
    		}
		}
    	if(this.pointsDeVie-puissanceCoup<=0) 
    	{
			this.pointsDeVie=0;
		}
    	else this.pointsDeVie-=puissanceCoup;
    }
    


 

 
   
    
}
