package control;

import constructions.*;
import protagonistes.Guerrier;


/*Cette classe perment de cr�er le labyrinthe*/

public class CreationLabyrintheController {
	
	private Labyrinthe labyrinthe;

	
	public void creerLabyrinthe(Guerrier guerrier)

	{ 

		labyrinthe = new Labyrinthe(guerrier);
		/*recu�rer une porte*/
		Porte porte;
		/*recuperer les salles reliant cette porte*/
		Salle salle1;
		Salle salle2;
		/*recuperer l'emplacement de cette porte par rapport au salles*/
		String emplacement1;
		String emplacement2;
		
		for (int i = 0; i < 9; i++) {
			
			porte=labyrinthe.getPortes(i);
			salle1=porte.getSalle1();
			salle2=porte.getSalle2();
            emplacement1=porte.getEmplacementPorteDansSalle1();
            emplacement2=porte.getEmplacementPorteDansSalle2();
            
            relierPorteASalle(porte,salle1 ,emplacement1);
            relierPorteASalle(porte, salle2,emplacement2);
            
		}

	}
	/*fonction qui relie les porte au salles*/
	
	private void relierPorteASalle(Porte porte, Salle salle, String emplacement) { 
		
		if (emplacement.equals("nord"))
			salle.setPorteNord(porte);
		else if (emplacement.equals("est"))
			salle.setPorteEst(porte);
		else if (emplacement.equals("sud"))
			salle.setPorteSud(porte);
		else if (emplacement.equals("ouest"))
			salle.setPorteOuest(porte);
	}
	
	
	public Labyrinthe getLabyrinthe() {
		return labyrinthe;
	}
	public Guerrier getGuerrier() {
		return labyrinthe.getGuerrier();
	}

    /*fonction pour tester la creation de la labyrinthe*/
	
	public void afficher() {
		for (int i = 0; i < 9; i++) {

			if (labyrinthe.getSalles(i).getPorteNord() != null) {
				System.out.println(
						"la salle " + i + " a la porte" + labyrinthe.getSalles(i).getPorteNord().getId() + " au nord");
			}

			if (labyrinthe.getSalles(i).getPorteSud() != null)
				System.out.println(
						"la salle " + i + " a la porte" + labyrinthe.getSalles(i).getPorteSud().getId() + " au sud");
			if (labyrinthe.getSalles(i).getPorteEst() != null)
				System.out.println(
						"la salle " + i + " a la porte" + labyrinthe.getSalles(i).getPorteEst().getId() + " � l'est ");
			if (labyrinthe.getSalles(i).getPorteOuest() != null)
				System.out.println("la salle " + i + " a la porte" + labyrinthe.getSalles(i).getPorteOuest().getId()
						+ " au ouest");
		}

	}

}
