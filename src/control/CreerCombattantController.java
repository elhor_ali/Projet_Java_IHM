package control;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.sun.org.apache.xerces.internal.impl.io.UTF8Reader;

import constructions.Labyrinthe;
import protagonistes.*;
import sun.text.normalizer.UTF16;
import tresor.Arme;
import tresor.ArmeNiveau1;
import tresor.ArmeNiveau2;
import tresor.ArmeNiveau3;
import tresor.Armure;
import tresor.ArmureNiveau1;
import tresor.ArmureNiveau2;
import tresor.ArmureNiveau3;


public class CreerCombattantController {

	private CreationLabyrintheController controlleurL;

	public CreerCombattantController(CreationLabyrintheController controlleurL) {
		super();
		this.controlleurL = controlleurL;
	}
	
/*creer un combattant d'apres son type*/
	public void creerCombattant(TypeCombattant typeCombattant, String nom) {
		/* permet de creer un combattant */
		switch (typeCombattant) {
		case GUERRIER: {
			Guerrier guerrier;
				if(nom=="@_RePrEnDre_@") {
					String [] guerrierT=lectureFichier("./txtFiles/infoGuerrier.txt");
					
					
					 guerrier =guerrierSaved( guerrierT);
				}else {
					 guerrier = new Guerrier(nom);
				}
			
			
            
			this.controlleurL.creerLabyrinthe(guerrier);

			break;

		}

		case MONSTRE: {
			Monstre monstre = new Monstre();

			break;
		}

		}

	}
	
	
	public Guerrier guerrierSaved(String [] guerrierT) {
		String nom= guerrierT[0];
		int puissance=Integer.parseInt(guerrierT[1]);
		int or = Integer.parseInt(guerrierT[2]);
		Arme arme;
		Armure armure;
		switch (guerrierT[3]) {
		case "niveau 1":
			  arme= new ArmeNiveau1();
			break;
        case "niveau 2":
        	 arme= new ArmeNiveau2();
			break;
        case "niveau 3":
        	arme= new ArmeNiveau3();
	       break;
	     default: arme=null;
		
		}
		
		switch (guerrierT[4]) {
		case "niveau 1":
			  armure= new ArmureNiveau1();
			break;
		case "niveau 2":
			  armure= new ArmureNiveau2();
			break;
		case "niveau 3":
			  armure= new ArmureNiveau3();
			break;
		default: armure=null;
		
		}
		
		Guerrier guerrier = new Guerrier(nom,puissance, or, arme,armure);
		return guerrier;
		
	}
	
	
	public void ecritureFichier(String nomFichier) {
		Guerrier guerrier=controlleurL.getGuerrier();
		String arme="null";
		String armure="null";
		try {
			PrintWriter writer= new PrintWriter(nomFichier);
			String nom=guerrier.getNomGuerrier();
			int puissance= guerrier.getPointsDeVie();
			int or=guerrier.getOr();
			
			if(guerrier.getArme()!=null) arme=guerrier.getArme().getNom();
			if(guerrier.getArmure()!=null) armure=guerrier.getArmure().getNom();
			
			writer.println(nom+","+puissance+","+or+","+arme+","+armure);
			writer.close();
		}catch(IOException e){System.err.println(e);}
		
	}
	
	public String[] lectureFichier(String nomFichier)
	{ /* mise � jour des valeur au cas d'une lecture fichier r�usit */
		String[] donneesGuerrier = new String[5];
		try
		{
			FileInputStream  fichier = new FileInputStream(nomFichier);   
			Scanner scanner = new Scanner(fichier);  
			
//			int nombrePortes=0;
//			if(scanner.hasNextLine()) this.nombreDeSalles=Integer.parseInt(scanner.nextLine());
//			if(scanner.hasNextLine()) nombrePortes = Integer.parseInt(scanner.nextLine());
			
			donneesGuerrier=scanner.nextLine().split(",");
			
			scanner.close();
			
		}
		catch(IOException e){System.err.println(e);}
		return donneesGuerrier;
	}
	
/*pour tester la cretation */
	
	
	public void afficher() {
		controlleurL.afficher();
	}

}
