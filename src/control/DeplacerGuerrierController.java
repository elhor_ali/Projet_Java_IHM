package control;

import java.util.Objects;
import constructions.Labyrinthe;
import constructions.Porte;

public class DeplacerGuerrierController {
	private Labyrinthe labyrinthe;

	public DeplacerGuerrierController(Labyrinthe labyrinthe) {
		super();
		this.labyrinthe = labyrinthe;

	}

	public Labyrinthe getLabyrinthe() {
		return labyrinthe;
	}

	public void setLabyrinthe(Labyrinthe labyrinthe) {
		this.labyrinthe = labyrinthe;
	}

	/* fonction de deplacement de guerrier
	 * cette fonction retourne null si on ne trouve pas de porte pour la direction que le guerrire a choisit
	 *  */
	public String deplacerGuerrier(int dirrection) {
		
		/*recuperer le choix du guerrier*/
		String dir = null;
		if (dirrection == 1)
			dir = "nord";
		else if (dirrection == 2)
			dir = "sud";
		else if (dirrection == 3)
			dir = "est";
		else if (dirrection == 4)
			dir = "ouest";
		else if (dirrection == 5)
			dir = "monstre";
		/*si le guerrier decide de ce deplacer au lieu de rester pour ce combattre*/
		if (dirrection != 5) {
			
			/*recuperer la porte qui envoie vers cette direction*/
			Porte porte = getPorteVers(dirrection);
			
			if (porte != null) {
				/*si la porte existe*/
				int nouveauEmpl = getNouveauEmplacement(porte);

				labyrinthe.setEmplacementGuerrier(nouveauEmpl);

				return dir;
			} else
				
				return null;

		} else {

			return dir;
		}

	}
	/*permet de recuperer la porte a prendre suivant la dirrection passee en param*/
	private Porte getPorteVers(int dirrection) {
		 
		Porte porte = null;
		if (dirrection == 1)
			porte = labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getPorteNord();
		else if (dirrection == 2)
			porte = labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getPorteSud();
		else if (dirrection == 3)
			porte = labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getPorteEst();
		else if (dirrection == 4)
			porte = labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getPorteOuest();
		return porte;
	}
	
	/* recupere le nouvel emplacement(piece) a partir de la porte passee en param */
	private int getNouveauEmplacement(Porte porte) {
		if (porte.getSalle1().getNumero() == labyrinthe.getEmplacementGuerrier()) {
			return porte.getSalle2().getNumero();
		} else
			return porte.getSalle1().getNumero();
	}

	

	public boolean getGuerrier() {
		if (labyrinthe.getGuerrier() != null) {
			return true;
		} else {
			return false;
		}
	}

}
