package control;

import java.util.Random;

import javax.swing.JLabel;

import constructions.Labyrinthe;

import protagonistes.Guerrier;
import protagonistes.Monstre;

public class CombattreController {
	private Labyrinthe labyrinthe;

	public CombattreController(Labyrinthe labyrinthe) {
		super();
		this.labyrinthe = labyrinthe;

	}
/* cette fonction permet de jouer un tour
 * un numero all�atoire entre 0 et 1 se genere
 * si c'est 0 le monstre attaque
 * si c'est 1 le guerreir attaque
 * */
	
	public String tourDattaque(JLabel jLabel) {
		String text = "";
		
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		Monstre monstre = labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getMonstre();
		/*on teste si un monstre existe deja dans la salle ou pas*/
		if (monstre != null) {
			int choix = this.generateNumber(0, 1);
          /*tour du monstre*/
			if (choix == 0)
				text += coupDeMonstre(guerrier, monstre);
			else
		 /*tour du guerrier*/
				text += coupDeGuerrier(monstre, guerrier);
			
			return text;

		} else
		{
			jLabel.setVisible(false);
			return "pas de monstre dans cette salle";

		}
			
	}
	public String pointsGuerrier() {
		String txt;
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		
		
		
		txt = "Votre point de vie est " + guerrier.getPointsDeVie();
		
		return txt;
	}
	
	public int pGuerrier() {
		
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		
		
		
		return guerrier.getPointsDeVie();
		
		
	}


	public int pMonstre() {
		
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		Monstre monstre =labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getMonstre();
		
		
		return monstre.getPointsDeVie();
		
		
	}
	public String pointsMonstre() {
		String txt;
		;
		Monstre monstre = labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getMonstre();
		
		txt = "Le point de vie du monstre est " + monstre.getPointsDeVie() ;

		
		return txt;
	}
	
	public int getPointDeVieMax(Guerrier guerrier) {
		return guerrier.getPointsDeVieMax();
	}
	

	
	public String coupDeGuerrier(Monstre monstre, Guerrier guerrier) {
		CreationTresorEtAttrib controlTresor= new CreationTresorEtAttrib(this.labyrinthe);
		//String text = "vous attaquez le monstre\n";
String text="";
		 guerrier.donnerCoup(monstre);
       /*verifier si le monstre est mort ou pas*/
		if (monstre.getPointsDeVie() == 0) {
			/*Le guerrier recup�re ses pointdevieMax +1*/
			restaurerEtBoosterGuerrier();
			
			//text = "Le monstre est mort.\n";
			text+= controlTresor.prendreRecompense();
			//on supprime le monstre de la salle
			this.labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).setMonstre(null);
			

		}
		return text;
	}
	
	public String coupDeMonstre(Guerrier guerrier, Monstre monstre) {
		String text;

		text = "\n le monstre vous attaque ";
		
		 monstre.donnerCoup(guerrier);
		/* verifier si le guerrier est mort ou pas*/
		if (guerrier.getPointsDeVie() == 0) {
			
			text +=  " Vou etes  est mort.\n";
			
			this.labyrinthe.setGuerrier(null);

		}
		return text;
	}



	public int generateNumber(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}


/*verifier si l'un des combatant est mort ou pas
 * 
 * cette fct est � modifier 
 * 
 * */
	public boolean estMort() {
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		Monstre monstre = labyrinthe.getSalles(labyrinthe.getEmplacementGuerrier()).getMonstre();


		if ( guerrier!=null && monstre!=null) {
			
			return false;
		} else
			return true;
	}

	

/* cette fonction permet au guerrier de recuperer ses points de vie +1*/
	public void restaurerEtBoosterGuerrier() {
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		guerrier.setPointsDeVieMax(guerrier.getPointsDeVieMax() + 1);
		guerrier.setPointsDeVie(guerrier.getPointsDeVieMax());
	}

	
}
